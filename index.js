const childProcess = require('child_process');
const version = require('./package.json').version;
const path = require('path');
const glob = require('glob');
const deployToolkit = require('@hmh/content-deployment-toolkit');
const fs = require('fs');
const mkdirp = require('mkdirp');
const tmp = require('tmp');

//---------------------------------------------------
//- config for commiting svn
//---------------------------------------------------
let options;

//---------------------------------------------------
//- cdmOptions config for cdm trigger only
//---------------------------------------------------
let cdmOptions;

//---------------------------------------------------
//- @class File  
//---------------------------------------------------
class File {
  constructor(source, destination) {
    this.source = source;
    this.destination = destination;
  }
}

//---------------------------------------------------
//- @class Svn 
//---------------------------------------------------
class Svn {
    static checkout(url, svnUsername, svnPassword) {
        const creds = svnUsername && svnPassword ? `--username ${svnUsername} --password ${svnPassword} ${url}` : url;
        return childProcess.execSync(`svn checkout ${creds}`);
    }

    static commit(message, svnUsername, svnPassword) {
        const creds = svnUsername && svnPassword ? `--username ${svnUsername} --password ${svnPassword} -m "${message}"` : `-m "${message}"`;
        return childProcess.execSync(`svn commit ${creds}`);
    }

    static add(files, options) {
        // Account for peg naming convention with files that have an @
        // example: file@2x2.png ==> file@2x2.png@
        // ref: http://svnbook.red-bean.com/en/1.5/svn.advanced.pegrevs.html
       const amendedFiles = files.map(destination => {
            destination = destination.replace(/^(.*@.*)$/g, '$1@');
            destination = '"' + destination + '"';
            return destination;
        });

        childProcess.execSync(`svn add ${options.join(' ')} ${amendedFiles.join(' ')}`);
    }
}

//---------------------------------------------------
//- file system class
//---------------------------------------------------
class FileSystem {
    static copy(source, dest) {
        mkdirp.sync(path.dirname(dest));
        fs.writeFileSync(dest, fs.readFileSync(source));
    }
    static createTempDir() {
        return tmp.dirSync({unsafeCleanup: true});
    }
    static writeFile(file, contents) {
        fs.writeFileSync(file, contents);
    }
}

//---------------------------------------------------
//- push to svn
//---------------------------------------------------
async function pushToSvn(files, commitMessage, platform, deploymentDescriptorFile, svnUsername, svnPassword) {
    // the cdm api wont deploy if the file doesnt end with .txt
    if (!deploymentDescriptorFile || deploymentDescriptorFile.match(/^.+\.txt$/) === null) {
        reject(new Error('The deployment descriptor file must end with .txt'));
    }
    await pushBuildToSvn(files, commitMessage, platform, svnUsername, svnPassword);
    return await pushDescriptorToSvn.apply(null, arguments);
}

function _batchCommitFiles(files, size, commitMessage, svnUsername, svnPassword) {
    const newSize = size || files.length;
    const batch = files.splice(0, newSize);
    console.log('batching....');

    batch.forEach(file => FileSystem.copy(file.source, file.destination));
    Svn.add(batch.map(file => file.destination), ['--force', '--parents']);
    Svn.commit(commitMessage, svnUsername, svnPassword);

    if (!files.length) return;
    _batchCommitFiles(files, newSize, commitMessage, svnUsername, svnPassword);

    return;
}


//---------------------------------------------------
//- push build files to svn
//---------------------------------------------------
function pushBuildToSvn(files, commitMessage, platform, svnUsername, svnPassword) {
    return new Promise((resolve, reject) => {
        const initialDir = process.cwd();
        const tmpContentDir = FileSystem.createTempDir();
        process.chdir(tmpContentDir.name);
        const contentPath = [platform.repositoryRoot, platform.contentPath].join('');
        Svn.checkout(contentPath, svnUsername, svnPassword);
        process.chdir(path.join(tmpContentDir.name, path.basename(contentPath)));

        const cloneFiles = JSON.parse(JSON.stringify(files));
        _batchCommitFiles(cloneFiles, options.batchConfig.batchSize, commitMessage, svnUsername, svnPassword);

        process.chdir(initialDir);
        tmpContentDir.removeCallback();
        resolve(true);
    });
}

//---------------------------------------------------
//- push descriptor to svn
//---------------------------------------------------
function pushDescriptorToSvn(files, commitMessage, platform, deploymentDescriptorFile, svnUsername, svnPassword) {
    return new Promise((resolve, rej) => {
        const initialDir = process.cwd();
        const tmpDescriptorDir = FileSystem.createTempDir();
        process.chdir(tmpDescriptorDir.name);
        const deploymentDescriptorPath = [platform.repositoryRoot, platform.deploymentDescriptorFolderPath].join('');

        Svn.checkout(deploymentDescriptorPath, svnUsername, svnPassword);
        process.chdir(path.join(tmpDescriptorDir.name, path.basename(deploymentDescriptorPath)));
        // remove '/data' from path
        const prefix = platform.contentPath.split('/').slice(2).join('/');
        FileSystem.writeFile(deploymentDescriptorFile, files.map(file => `/${prefix}/${file.destination}`).join('\n'));
        Svn.add([deploymentDescriptorFile], ['--force', '--parents']);
        Svn.commit(commitMessage, svnUsername, svnPassword);
        process.chdir(initialDir);
        tmpDescriptorDir.removeCallback();
        resolve(deploymentDescriptorFile);
    });
  
}

//---------------------------------------------------
//- filter file in case ignore is set as well
//---------------------------------------------------
function _filterFiles(options) {
    let files = [];
    const root = path.join.apply(null, [process.cwd(), options.files.root].filter(string => string !== undefined));

    const filteredFiles = options.files.sources
      .map(pattern => glob.sync(pattern, { cwd: root, nodir: true }))
      .reduce((previous, current) => previous.concat(current), []);

    filteredFiles.forEach((file) => {
        if (options.files.copy && options.files.copy[file]) {
            options.files.copy[file].forEach((newFile) => {
                files.push(new File(path.join(root, file), newFile));
            });
        }

        if (!options.files.ignore || options.files.ignore.indexOf(file) < 0) {
            let f = new File(path.join(root, file), file); 
            files.push(f);
        }

    });
    if(options.files.removeRoot) {
        files = files.map((f) => {
            f.destination = f.destination.replace(/^\/?[^\/]*\/(.*)$/gi, '$1');
            return f;
        });
    }

    return files;
}

//---------------------------------------------------
//- generate unique id
//---------------------------------------------------
function generateUuid() {
    return (function fn(name) {
        name.push(Math.random().toString(32).replace(/.*(.{6})$/g, '$1'));
        return name.length === 4 ? name : fn(name);
    })([]).join('-');
}

//---------------------------------------------------
//- trigger CDM job
//---------------------------------------------------
function triggerCdm (cdmOptions) {
    if(!cdmOptions.payload.deploymentDescriptorFile) {
        cdmOptions.payload.deploymentDescriptor = cdmOptions.descriptorRoot + cdmOptions.descriptorFileName;
    }
    console.log(cdmOptions.payload);
    return new Promise(resolve => {
        deployToolkit.deploy(cdmOptions.payload, {
            username: cdmOptions.cdmApiUsername,
            password: cdmOptions.cdmApiPassword
        })
        .then(() => {
            console.log('SUCCESS - Deployment complete')
            resolve(cdmOptions.payload.deploymentDescriptor);
        })
        .catch((err) => console.log('ERROR', err));
        });
}

//---------------------------------------------------
//- deploy to cert
//---------------------------------------------------
async function deployToCert(config) {
    options = config.svn;
    cdmOptions = config.cdm;

    let files = _filterFiles(options);
    const deploymentDescriptorFileName = options.deploymentDescriptorFileName || `${generateUuid()}.txt`;
    await pushToSvn(
        files,
        options.commitMessage, 
        options.platforms[0],
        deploymentDescriptorFileName,
        options.svnUsername, 
        options.svnPassword
    );

    cdmOptions.descriptorFileName = deploymentDescriptorFileName;
    cdmOptions.payload.deploymentDescriptor = cdmOptions.descriptorRoot + cdmOptions.descriptorFileName;
    return await triggerCdm(cdmOptions); 
}



module.exports = {
    deployToCert,
    triggerCdm,
    emitter: deployToolkit.emitter
}